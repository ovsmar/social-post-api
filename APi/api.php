<?php
require 'vendor/autoload.php'; // Composer auto-loader using Guzzle. See https://docs.guzzlephp.org/en/stable/overview.html

function share($text, $urlImg){
    $client = new GuzzleHttp\Client(); // Use the HTTP library of your choice
    $res = $client->request(
        'POST',
        'https://app.ayrshare.com/api/post',
        [
            'headers' => [
                'Content-Type'      => 'application/json',
                'Authorization'     => 'Bearer Q7C28H8-N454TFQ-J0YKK5A-H4SMZ0P'
            ],
            'json' => [
                'post' => $text, // required
                'platforms' => ['facebook', 'instagram'], // required
                'mediaUrls' => [$urlImg], // optional
            ]
        ]
    );

    echo json_encode(json_decode($res->getBody()), JSON_PRETTY_PRINT); 
}