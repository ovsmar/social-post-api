DROP database if exists Api;
CREATE DATABASE Api;

USE Api;

CREATE TABLE post (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(25),
    imgURL VARCHAR(255)
 

);

GRANT ALL PRIVILEGES ON Api.* TO 'ovsep'@'localhost';

